FROM php:7.2-apache

MAINTAINER Ricky Mathew

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

## COPY . /var/www/html

#Install git
RUN apt-get update \
    && apt-get install -y git
 
#Change directory and clone Qxf2 Public POM repo
RUN mkdir /var/www/html \
    && cd /var/www/html \
    && git clone https://github.com/rickymathewk/Typo3-extension-site-statistics.git

ADD vhost.conf /etc/apache2/sites-available/000-default.conf

WORKDIR /var/www/html

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN docker-php-ext-install mbstring pdo pdo_mysql \
&& chown -R www-data:www-data /var/www/html \ 
&& a2enmod rewrite && a2enmod proxy && a2enmod proxy_http && a2enmod proxy_connect && service apache2 restart
