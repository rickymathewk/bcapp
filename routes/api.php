<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Register and login
Route::post('login', 'PassportController@login');
Route::post('register', 'PassportController@register');

//Api routes needs authentication
Route::middleware('auth:api')->group(function () {
	Route::get('user', 'PassportController@details');
	Route::get('getAllTips', 'ApiController@index');
	Route::get('getTip/{guid}', 'ApiController@show');
	Route::post('storeTip', 'ApiController@store');
	Route::post('updateTip/{guid}', 'ApiController@update');
	Route::delete('deleteTip/{guid}', 'ApiController@delete');
});