<!doctype html>
<html lang="<?php echo e(app()->getLocale()); ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #000;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }
            .position-ref {
                position: relative;
            }
            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }
            .content {
                text-align: center;
            }
            .form-group{text-align: left;}
            .form-control{color: #000!important;}
            .btn{margin-right: 5px;}
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                   <h3>Create Tip</h3>
                   <?php echo e(Form::open(array('url'=>'storeTip','method' => 'post'))); ?>                                  
                   <div class="form-group">
                      <label>Title</label>
                      <?php echo e(Form::text('title','', array('class' => 'form-control', 'placeholder' => 'Title'))); ?>

                   </div>
                   <div class="form-group">
                      <label>Description</label>
                      <?php echo e(Form::textarea('desctiption','', array('class' => 'form-control', 'rows' => 2, 'cols' => 40))); ?>

                   </div>
                   <?php echo e(Form::submit('Create',array('id'=>'','class'=>'btn btn-primary pull-left btn-reset'))); ?>

                   <?php echo e(Form::reset('Clear',array('id'=>'','class'=>'btn btn-danger pull-left'))); ?>

                   <?php echo e(Form::close()); ?>

            </div>
        </div>
    </body>
</html>
