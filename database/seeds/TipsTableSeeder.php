<?php

use Illuminate\Database\Seeder;

class TipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tips')->insert([
            [
            'title' => 'Manchester United vs Crystal Palace',
            'description' => 'After getting pumped off their rivals and being shown up for how far behind they are, Man Utd will be keen to get a win under their belt again. '
            ],
            [
            'title' => 'Tottenham vs Chelsea',
            'description' => 'Both Tottenham and Chelsea are having good seasons where they sit 4th and 3rd in the league. They are very tight on the form table with Spurs just edging it ever so slightly.'
            ],
        ]);
    }
}
