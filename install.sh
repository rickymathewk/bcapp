#!/usr/bin/env bash

set -e

 [ -f ".env" ] || $(echo Please make an .env file; exit 1)
export $(cat .env | grep -v ^# | xargs);
echo Starting services
docker-compose up -d

# until docker-compose exec mysql mysql -h $DB_HOST -u $DB_USERNAME -p$DB_PASSWORD -D $DB_DATABASE --silent -e "show databases;"
# do
#   echo "Waiting for database connection....please dont Exit on Error!"
#   sleep 5
# done

# echo "database connected!..."

rm -f bootstrap/cache/*.php

echo Seeding database
docker-compose exec app php artisan key:generate && echo key generated
docker-compose exec app php artisan migrate && echo Database migrated
docker-compose exec app php artisan db:seed && echo Database seeded
docker-compose exec app php artisan passport:install --force