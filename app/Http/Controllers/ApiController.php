<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Tips\StoreTipRequest;
use App\Http\Requests\Tips\UpdateTipRequest;
use App\Http\Requests\Tips\ShowTipRequest;
use App\Http\Requests\Tips\DeleteTipRequest;
use App\Http\Requests\Tips\GetallTipsRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use App\Tip;
use App\Transformers\TipTransformer;

class ApiController extends Controller
{
	/**
	 * Get all tips
	 * @return type
	 */

    public function index(GetallTipsRequest $request ,$limit=250)
    {
       $allTips = Tip::orderby('guid')->paginate($limit);
       
       if($allTips->isEmpty()){
         return response()->json(['status'=>'No tip found'], 200);
       }
       return fractal($allTips, new TipTransformer())->respond(200, ['Content-Type' => 'application/json','Accept' => 'application/json',]);
    }
    
    /**
     * Show details of a specific tip
     * @param type $id 
     * @return type
     */
    public function show($guid, ShowTipRequest $request)
    {
         $tipDetails = Tip::where('guid', $guid)->get();
         $response = [];
         $status = false;
         $response['status'] = &$status;            
         if(!$tipDetails->isEmpty()){
            $status = true;
            $response['Tip'] = fractal($tipDetails, new TipTransformer())->toArray();
         }
         return response()->json($response, 200);
    }
    
    /**
     * Add a new tip
     * @return type
     */
    public function store(StoreTipRequest $request)
    {   
        $tip = new Tip;
        $validator = $this->validator($request->all(),$request->rules(),$request->messages());
        if ($validator->fails()) {
            return json_encode(array('status'=>'Validation error','Error' =>  $validator->errors()));
        }
        $tip->title =  $request->title;
        $tip->description = $request->description;
        $tip->save();
        $tipDetails = fractal($tip, new TipTransformer())->toArray();
        return response()->json(['Status'=>"Tip added.",'Tip' => $tipDetails], 200);
    }
    
    /**
     * Updates a tip
     * @param type $id 
     * @return type
     */
    public function update($guid,UpdateTipRequest $request)
    {
        $tip = Tip::where('guid', $guid)->first();
        $response = [];
        $status = false;
        $response['status'] = &$status;
       
        if(empty($request->all())){
            $status = "Empty parameters";
        }
        else{
            if(count($tip)){
               $tip->fill($request->all())->save();
               $status = "Tip updated";
               $response['Tip'] = fractal($tip, new TipTransformer())->toArray();
           }
            else{
              $status = "Tip not found";
           }
        }
        
        return response()->json($response, 200);
    }
    
    /**
     * Deletes a tip
     * @param type $id 
     * @return type
     */
    public function delete($guid, DeleteTipRequest $request)
    {
        $tip = Tip::where('guid', $guid)->first();
        $response = [];
        $status = false;
        $message = 'Tip not found';
        $response['status'] = &$status;
        $response['message'] = &$message;                             
        
        if($tip){
          $tip->delete();
          $message ="Tip ".$guid." deleted";
          $status = true;
        }
        return response()->json($response, 200);
    }

    /**
     * Validates a request
     *
     */
    public function validator($data,$rules,$messages)
    {
        return Validator::make($data,$rules,$messages);
    }
}
